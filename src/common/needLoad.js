import {
  Button,
  Avatar,
  Input,
  Form,
  FormItem
} from 'element-ui';

const CompArray = [
  Button,
  Avatar,
  Input,
  Form,
  FormItem
]
export default function(Vue){
  CompArray.forEach(item => {
    Vue.use(item)
  })
}
