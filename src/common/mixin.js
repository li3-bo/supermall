import {debounce} from './utils'
import BackTop from '../components/comtent/backtop/BackTop'

export const itemListenerMixin = {
  data(){
    return{
      itemImgListener:null,
      refresh:null
    }
  },
  mounted(){
    //1.图片加载完成的时间监听
    // this.$refs.scroll.refresh对这个函数进行防抖操作
    this.refresh = debounce(this.$refs.scroll.refresh,800);

    //对监听事件进行保存
    this.itemImgListener = () => {
      this.refresh()
    }

    //接受图片完成加载后发射的
    this.$bus.$on('itemImageLoad',this.itemImgListener )
    // console.log('我是混入中的内容');
  }
}


export const backTopMixin = {
  components:{
    BackTop
  },
  data(){
    return{
      isShowBackTop:false,
    }
  },
  methods:{
    backClick(){
      //  点击回到顶部
      this.$refs.scroll.scrollTo(0,0,500)
    },
    isShowBack(position){
      this.isShowBackTop = -position.y > 1000
    }
  }
}
