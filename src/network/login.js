import {request2} from './request';

export function getLoginInfo(name,password){
  return request2({
    url:'/login',
    method:'POST',
    data:{
      name,
      password,
    }
  })
}

export function registerNumber(name,password){
  return request2({
    url:'/user',
    method:'POST',
    data:{
      name,
      password
    }
  })
}

