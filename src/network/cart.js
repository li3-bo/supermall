import {request2} from './request';

export function orderAdd(token,orderInfo){
  console.log("222")
  return request2({
    url:"/order",
    method:"POST",
    headers:{
      Authorization:`Bearer ${token}`
    },
    data:{
      orderInfo
    }
  })
}

export function orderNumber(token){
  return request2({
    url:"/order",
    method:"GET",
    headers:{
      Authorization:`Bearer ${token}`
    }
  })
}
