import axios from "axios";
import {request} from "./request";

export function getHomeMultidata(){
  // console.log('1111111')
  // axios({
  //   url:'http://localhost:8000/supermall/home/multidata'
  // }).then(res => {
  //   console.log(res)
  // })
  return request({
    url:'/home/multidata'
  })
}

export function getHomeGoods(type,page){
  return request({
    url:'/home/data',
    // url:'/v1/categories',
    params:{
      type,
      page
    }
  })
}
