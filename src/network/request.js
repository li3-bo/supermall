//网络请求，从接口处拿东西，
import axios from 'axios' //先引入安装的axios。 npm install axios --save
export function request(config) {
  //1.创建axios的实例
  const instance1 = axios.create({
    baseURL:'http://152.136.185.210:7878/api/hy66',
    timeout:5000
  })
  //2.axios的拦截器
  //2.1.请求拦截的作用
  instance1.interceptors.request.use(config => {
    return config
  },err => {
    // console.log(err);
  })
  //2.2响应拦截
  instance1.interceptors.response.use(res => {
    return res.data //从接口里只拿一个data,引用该函数返回的res也只有data
  },err => {
    // console.log(err);
  })
  //3.发送真正的网路请求
  return instance1(config) //返回的是一个Promise，别的地方调用这个函数的时候可以跟上.then
}

export function request2(config) {
  // 1.创建axios的实例
  const instance2 = axios.create({
    baseURL:"http://localhost:8000",
    // baseURL:"http://120.27.231.99:8001",
    timeout:5000
  })

  // 请求拦截
  instance2.interceptors.request.use(config => {

    // let token = window.localStorage.getItem('token')
    // if(token){
    //   token = JSON.parse(token)
    //   console.log(config)
    //   // console.log(token)
    //   config.headers.Authorization = `Bearer ${token}`
    // }
    return config
  },err => {
    console.log('请求错误',err)
    return config
  })

  // 响应拦截
  instance2.interceptors.response.use(res => {
    return res
  },err => {
    console.log('请求错误',err)
    return err
  })

  return instance2(config)

}

