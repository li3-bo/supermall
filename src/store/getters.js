export default{
  cartLength(state){
    return state.cartList.length
  },
  cartList(state){
    return state.cartList
  },
  unOrderNumber(state){
    const result = state.orderInfo.filter(item => item.status !== 1).length
    // console.log(result)
    return result;
  },
  alOrderNumber(state){
    return state.orderInfo.filter(item => item.status === 1).length
  }
}
