import {
  ADD_COUNTER,
  ADD_TO_CART
} from './mutations-type'

import {getLoginInfo} from '../network/login';
import {orderNumber} from '../network/cart';

export default{
  addCart(context,payload){
    return new Promise((resolve,reject) => {
      //context相当于{state,commit}
      //1.查找之前数组中是否有该商品
      let oldProduct = context.state.cartList.find(item => item.iid === payload.iid);

      //2.判断 oldProduct
      if(oldProduct){//商品数量加一
        // oldProduct.count += 1
        context.commit(ADD_COUNTER,oldProduct)
        resolve('当前商品数量+1')
      } else{//添加新的商品
        payload.count = 1
        // context.state.cartList.push(payload)
        context.commit(ADD_TO_CART,payload)
        resolve('添加了新的商品')
      }
    })
  },
  async getLoginInfo({commit},payload){
    // console.log(context)  //{commit,dispatch,getters,state,rootGetters,rootState}
    return new Promise((resolve,reject) => {
      const {name,password} = payload

      getLoginInfo(name,password).then(res => {
        resolve(res.status)
        commit('userInfo',res.data)
      }).catch(err => {
        console.log('登录失败',err)
      })

    })
  },
  async getOrderNumber({state,commit}){
    // console.log("11111111111111")
    const token = state.token
    const {data} = await orderNumber(token)
    commit('getOrderNumber',data)
  }
}
