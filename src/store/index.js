import Vue from 'vue'
import Vuex from 'vuex'

import mutations from './mutations'
import actions from './actions'
import getters from './getters'

//1.安装插件
Vue.use(Vuex)

//2.创建Store对象
const store =new Vuex.Store({
  state:{
    cartList:[],
    token:'',
    userInfo:{},
    orderInfo:[]
  },
  mutations,//只能通过mutations修改state，通过commit
  actions,//用来异步加载，通过dispatch
  getters//计算属性
});

//3.挂在到vue实例上,main.js文件中
export default store
