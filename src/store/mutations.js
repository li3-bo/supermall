import {
  ADD_COUNTER,
  ADD_TO_CART
} from './mutations-type'


export default {
  //mutations唯一的目的就是修改state中的状态
  //mutations中的每个方法尽可能完成的事情比较单一一点
  [ADD_COUNTER](state,payload){
    payload.count++
  },
  [ADD_TO_CART](state,payload){
    payload.checked = false
    payload.count = 1;
    state.cartList.push(payload)
  },
  userInfo(state,payload){
    state.token = payload.token
    // window.localStorage.setItem("token",JSON.stringify(payload.token))
    state.userInfo = {
      id:payload.id,
      name:payload.name
    }
  },
  alterChecked(state,payload){
    state.cartList[payload.index] = payload.checked
  },
  cartListDetail(state,payload){
    state.cartList = state.cartList.filter((item) => {
      return item.checked != true
    })
  },
  getOrderNumber(state,payload){
    state.orderInfo = payload
  }
}
