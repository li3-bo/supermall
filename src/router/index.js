import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

// const Home = () => import('../views/home/Home')
// const Cary = () => import('../views/cart/Cary')
// const Category = () => import('../views/category/Category')
// const Profile = () => import('../views/profile/Profile')
// const Detail = () => import('../views/detail/Detail')
// const Login = () => import('../views/login/Login')

import Home from '@/views/home/Home'
import Cary from '../views/cart/Cary'
import Category from '../views/category/Category'
import Profile from '../views/profile/Profile'
import Detail from '../views/detail/Detail'
import Login from '../views/login/Login'
import Register from '../views/login/register.vue'

const routes = [
  {
    path:'',
    redirect:'/home'   //重定位
  },
  {
    path:'/home',
    component:Home
  },
  {
    path:'/cary',
    component:Cary
  },
  {
    path:'/profile',
    component:Profile
  },
  {
    path:'/category',
    component:Category
  },
  {
    path:'/detail/:id',
    component:Detail
  },
  {
    path:'/login',
    component:Login,
  },
  {
    path:"/register",
    component:Register
  }
  ]
const router = new Router({
//  配置路由和组件之间的应用关系
  routes,
  // mode:'history',  //hash模式,就是URL地址里的变化跟正常网页一样
  mode:'hash'  //hash模式,就是URL地址里的变化跟正常网页一样

})
export  default router
