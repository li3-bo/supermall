import Vue from 'vue'
import App from './App.vue'
import router from './router/index'  //路由
import store from './store/index'  //vuex
import 'element-ui/lib/theme-chalk/index.css';

import toast from './components/common/toast/index' //注册组件toast

import FastClick from 'fastclick' //解决移动端延迟300ms，先npm install fastclick --save
import VueLazyLoad from 'vue-lazyload'//图片懒加载，安装npm install vue-lazyload,引用后再把src换成v-lazy

import CompArray from './common/needLoad';

CompArray(Vue)

Vue.config.productionTip = false


//添加事件总线对象,在vue的原型对线里添加属性
Vue.prototype.$bus = new Vue();
//安装toast插件
//文件一启动机会调用toast函数，在调用的时候还会传入vue
Vue.use(toast)

//解决移动端延迟300ms
FastClick.attach(document.body)

//使用懒加载的插件
Vue.use(VueLazyLoad)

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
