// 配置别名
module.exports = {
    publicPath:'./',
    configureWebpack:{
        resolve:{
            alias:{
              'assets': '@/assets',
              'common': '@/common',
              'components': '@/components',
              'network': '@/network',
              'views': '@/views'
            }
        }
    }
    // devServer: {
    //   proxy: {
    //       '/api': {
    //         target: 'http://120.27.231.99:8001',   //node.js服务器运行的地址
    //         ws: true,
    //         changeOrigin: true,
    //         pathRewrite: {
    //             '^/api': 'http://120.27.231.99:8001'  //路径重写
    //           }
    //       },
    //   }
    // }
}
